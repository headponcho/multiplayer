﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Combat : NetworkBehaviour {

    [SyncVar] public int health = maxHealth;

    public const int maxHealth = 100;
    public bool destroyOnDeath;

    public void TakeDamage(int amount) {
        if (!isServer) { return; }

        health -= amount;
        if (health < 0) {
            health = 0;

            if (destroyOnDeath) {
                Destroy(gameObject);
            } else {
                health = maxHealth;
                // called on the server, will be invoked on the clients
                RpcRespawn();
            }
        }
    }

    [ClientRpc]
    void RpcRespawn() {
        if (isLocalPlayer) {
            // move back to zero location
            transform.position = Vector3.zero;
        }
    }
}
